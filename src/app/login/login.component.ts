import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public submitted = false;
  public username = '';
  public password = '';
  public formPage: FormGroup;
  public errorMessage: any;
  public errorValue = false;
  constructor(private router: Router,
    private formBuilder: FormBuilder,) {
    this.formPage = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

  }

  ngOnInit(): void {
  }


  login() {
    if (this.formPage.invalid) {

      this.submitted = true;
    }
    else if (this.formPage.value.username == 'lucky' && this.formPage.value.password == '1234') {
      this.router.navigate(['/dashboard']);
    }
    else {
      alert("invalid details");
    }

  }
  get f() { return this.formPage.controls; }

}
