import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EmployeedataService } from '../services/employeedata.service';


declare var $: any;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public submitted = false;
  public addPage: FormGroup;
  public updatePage:FormGroup;
  public listUsers: any;
  readonlyFlag: any;
  userdetails: any;
  newUsers: any;
  public data: any;
  public id: any;
  public genderData = [];
  dataLoaded: boolean = false;
  public userObj = '';
  userId: String = '';
  maxDate: Date;
  public saveFlag = true;
  public updateFlag =true;
  constructor(private activateRoute: ActivatedRoute, private router: Router, private _snackBar: MatSnackBar, private employeedataService: EmployeedataService, private formBuilder: FormBuilder,) {
    this.maxDate = new Date();

    this.addPage = this.formBuilder.group({
      id: [''],
      name: ['', Validators.required],
      MobileNumber: ['', Validators.required],
      Email: ['', Validators.required],
      DOB: ['', Validators.required],
      Description: ['', Validators.required],
      gender: ['1', Validators.required],
      country: ['', Validators.required],
      //Language: ['', Validators.required]

    });
    this.updatePage = this.formBuilder.group({
      id: [''],
      name: ['', Validators.required],
      MobileNumber: ['', Validators.required],
      Email: ['', Validators.required],
      // DOB: ['', Validators.required],
      // Description: ['', Validators.required],
      // gender: ['1', Validators.required],
      // country: ['', Validators.required],
      // Language: ['', Validators.required]

    });
  }

  ngOnInit(): void {

    this.getUsers();

  }

  getUsers() {
    this.employeedataService.listusers().subscribe(
      empResponse => {
        this.listUsers = empResponse;
        console.log(empResponse);
      }
    );
  }

  addPopup() {
    this.submitted = false;
      this.updateFlag = false; 
      this.addPage.reset();
   
    $('#exampleModalCenter').appendTo("body").modal("show")
    //this.getUsers();
  }


  add() {

    if (this.addPage.invalid) {
      this.submitted = true;
      //alert('form invalid')
      return
    }


    this.employeedataService.addUser(this.addPage.value).subscribe(
      response => {

        $('#exampleModalCenter').modal('hide');
        this.getUsers();
      }, err => {
        console.log("error");
      });
  }

  edit(id: any) {
    this.id = id;
    if (id) {
      this.saveFlag = false; 
       
    }
    
    //this.submitted = false;
    console.log(this.id)

    $('#exampleModalCenter').appendTo("body").modal('show')
    if (this.id !== '') {
      this.employeedataService.viewUser(this.id)
        .toPromise()
        .then(data => {
          this.userdetails = data;
          Object.assign(this.userdetails, data);
          console.log(this.userdetails.data[0]);
          // this.addPage.patchValue({ Id: this.userdetails.id, name: this.userdetails.name, Email: this.userdetails.Email, MobileNumber: this.userdetails.MobileNumber });
          this.addPage.patchValue(this.userdetails.data[0]);
          this.addPage.get('DOB')?.setValue('2020-10-10');
          console.log("success")
        });
    }
  }

  update() {
    this.addPage.get('id')?.setValue(this.id);
    console.log(this.addPage.value)
    this.employeedataService.editUser(this.addPage.value).subscribe(
      data => {
        console.log(data)
        // if(this.addPage.value.id == this.id){
          $('#exampleModalCenter').modal('hide');
          this.getUsers();
         //  this.addPage.value.id.name = this.name;
        // }
        return true;
      });
    // console.log(this.addPage.value);

  }

  
  deletePopup(id: any) {
    this.id = id;
    $('#exampleModal').appendTo("body")
  }
  delete() {

    this.employeedataService.delete(this.id).subscribe(
      data => {
        console.log("id")
        $('#exampleModal').modal('hide');
        this.getUsers();
        // console.log("delete");



      }, err => {
        console.log("error");
      });

  }

  get f3() { return this.updatePage.controls; }

  get f2() { return this.addPage.controls; }
}


