import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class EmployeedataService {

  baseUrl: string ='http://localhost:3003/api/users';
  constructor(private http:HttpClient) { }

  listusers()
 {
    return this.http.get(this.baseUrl)
  
  }
  addUser(userObj:any)
  {
    return this.http.post(this.baseUrl + '/adduser',userObj);
  }
  viewUser(id:string)
  {
    return this.http.get(this.baseUrl +'/'+id);
 
  }
  delete(id:any)
  {
   return this.http.delete(this.baseUrl + '/deluser/'+ id);
  }
  editUser(userObj:any)
  {
    return this.http.put(this.baseUrl + '/update' ,userObj);
  }
}
